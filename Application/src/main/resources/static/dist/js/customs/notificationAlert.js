$(function(){
	"use strict";
	if(successfulUpdate == true){
	   $.toast({
		heading: 'NOTIFICATION',
		text: 'Mise a jour avec success.',
		position: 'top-right',
		loaderBg:'#ff6849',
		icon: 'info',
		hideAfter: 3000, 
		stack: 6
	  });
	}else if(successfulCreate == true){
	   $.toast({
		heading: 'NOTIFICATION',
		text: 'Creer avec success.',
		position: 'top-right',
		loaderBg:'#ff6849',
		icon: 'info',
		hideAfter: 3000, 
		stack: 6
	  });
	}
});