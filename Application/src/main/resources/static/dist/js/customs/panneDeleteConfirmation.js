function panneDeleteConfirmation(id) {
	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
			confirmButton: 'btn btn-success',
			cancelButton: 'mr-2 btn btn-danger'
		},
		buttonsStyling: false,
	})

	swalWithBootstrapButtons.fire({
		title: 'Etes vous sure?',
		text: "Vous ne pourrez revenir en arriere!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Oui, supprimer!',
		cancelButtonText: 'Non, annuler!',
		reverseButtons: true
	}).then((result) => {
		if (result.value) {
			swalWithBootstrapButtons.fire(
				'Supprimer!',
				'Panne a ete supprimer.',
				'success'
			),
			window.location.replace("/panne/deletePanne/"+id);
		} else if (
			// Read more about handling dismissals
			result.dismiss === Swal.DismissReason.cancel
		) {
			swalWithBootstrapButtons.fire(
				'Annuler',
				'Supression annuler',
				'error'
			)
		}
	})
}