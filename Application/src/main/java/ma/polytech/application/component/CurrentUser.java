package ma.polytech.application.component;

import java.math.BigInteger;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;

import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.Poste;
import ma.polytech.application.entity.User;
import ma.polytech.application.repository.PosteRepository;
import ma.polytech.application.repository.UserRepository;
import ma.polytech.application.service.EmployeService;
import ma.polytech.application.service.PosteService;
import ma.polytech.application.service.UserServiceImp;
import ma.polytech.application.validator.EmployeValidator;

public class CurrentUser {
	
	@Autowired
	private EmployeService employeService;
	
	@Autowired
	private UserServiceImp userService;
	
	@Autowired
	private PosteRepository posteRepository;
	
	public CurrentUser(EmployeService employeService) {
		this.employeService = employeService;
	}

	public CurrentUser(UserServiceImp userService) {
		this.userService = userService;
	}
	
	public CurrentUser(EmployeService employeService, UserServiceImp userService) {
		this.userService = userService;
		this.employeService = employeService;
	}

	public Employe getCurrentUser() {
		Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
		String email = loggedInUser.getName();
		System.out.println(email);
		Employe empLogged = employeService.findByEmail(email);
		if(empLogged == null)
			return createTestEmploye();
		return empLogged; // this is the current logged user
	}
	
			
//	@EventListener(ApplicationReadyEvent.class)
	public Employe createTestEmploye() {
		Employe employeTest = employeService.findByUsername("default");
		if(employeTest == null) {
			return employeService.save(new Employe("default", "default", "default", "default@gmail.com", "default", new Date(), new BigInteger("76789292")));
		}

		/* CREATE DEFAULT EMPLOYE ADMIN */
		if(employeService.findByUsername("default_admin") == null || employeService.findByEmail("yattdeveloper@gmail.com") == null) {
			Employe adminNew = new Employe("default_admin", "default_admin", "default_admin", "yattdeveloper@gmail.com", "76789292", new Date(), new BigInteger("76789292"));
			if(posteRepository.findByNom("Responsable") == null)
				posteRepository.save(new Poste("Responsable"));
			adminNew.setPoste(posteRepository.findByNom("Responsable"));
			employeService.save(adminNew);
		} else if (!employeService.findByUsername("default_admin").postNom()
				.contentEquals("Responsable")) { /* UPDATE ADMIN POSTE IF CHANGED */
			if(posteRepository.findByNom("Responsable") == null)
				posteRepository.save(new Poste("Responsable"));
			employeService.findByUsername("default_admin").setPoste(posteRepository.findByNom("Responsable"));
			BindingResult bindingResult = null;
			employeService.update(employeService.findByUsername("default_admin"), bindingResult);
		}
		
		/* CREATE DEFAULT USER ADMIN */
		User userdb = userService.findByUsername("default_admin");
		if(userdb == null) {
			User user = new User("default_admin", "yattdeveloper@gmail.com", "76789292");
			userService.save(user);
		}
		return employeTest;
	}

}
