package ma.polytech.application.exception;

import java.io.FileNotFoundException;

public class MyFileNotFoundException extends FileNotFoundException {
	
	public MyFileNotFoundException(String msg) {
		super(msg);
	}

}
