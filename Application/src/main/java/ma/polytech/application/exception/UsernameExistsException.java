package ma.polytech.application.exception;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UsernameExistsException extends UsernameNotFoundException {

	public UsernameExistsException(String msg) {
		super(msg);
		// TODO Auto-generated constructor stub
	}

}
