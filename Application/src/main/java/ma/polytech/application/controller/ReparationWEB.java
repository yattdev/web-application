package ma.polytech.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ma.polytech.application.component.CurrentUser;
import ma.polytech.application.entity.Panne;
import ma.polytech.application.entity.Reparation;
import ma.polytech.application.repository.EnginRepository;
import ma.polytech.application.repository.MecanicienRepository;
import ma.polytech.application.repository.PanneRepository;
import ma.polytech.application.service.EmployeService;
import ma.polytech.application.service.PanneService;
import ma.polytech.application.service.ReparationService;
import ma.polytech.application.service.UserServiceImp;

@Controller
public class ReparationWEB {
	
	@Autowired
	private ReparationService service;
	
	@Autowired
	private PanneService panneService;
	
	@Autowired
	private MecanicienRepository mecano;
	
	@Autowired
	private EmployeService employeService;
	
	@Autowired
	private UserServiceImp userService;
	
	private boolean successfulUpdate = false;
	private boolean successfulCreate = false;
	private boolean modalShow = false;
	private boolean nomError = false;
	private boolean dateError = false;
	
	@RequestMapping("/reparation/tousreparations")
	public String findAllReparation(Model model) {
	
		CurrentUser currentUser = new CurrentUser(employeService, userService);
		model.addAttribute("currentUser", currentUser.getCurrentUser());
		List<Reparation> appros = service.getAll();
		model.addAttribute("reparations", appros);
		model.addAttribute("pannelist", panneService.getAllPanne());
		model.addAttribute("mecanicienlist", mecano.findAll());
		model.addAttribute("successfulUpdate", successfulUpdate);
		model.addAttribute("successfulCreate", successfulCreate);
		model.addAttribute("modalShow", modalShow);
		model.addAttribute("nom", nomError);
		model.addAttribute("reparation", new Reparation());
		successfulUpdate = false;
		successfulCreate = false;
		modalShow = false;
		nomError = false;
		dateError = false;
		return "ReparationTemplates/allReparation";
	}
	
//	@RequestMapping("/reparation/nouveauReparation/")
//	public String nouveauReparation(Model model) {
//		Reparation newReparation = new Reparation();
//		model.addAttribute("pannelist", panneService.getAllPanne());
//		model.addAttribute("mecanicienlist", mecano.findAll());
//		model.addAttribute("reparation", newReparation);
//		return "ReparationTemplates/nouveauReparation";
//	}
//	
//	@RequestMapping("/reparation/newReparation")
//	public String nouvReparation(Model model) {
//		Reparation newReparation = new Reparation();
//		model.addAttribute("pannelist", panneService.getAllPanne());
//		model.addAttribute("mecanicienlist", mecano.findAll());
//		model.addAttribute("reparation", newReparation);
//		
//		return "ReparationTemplates/test";
//	}
	
//	@RequestMapping(value="/reparation/saveReparation", method = RequestMethod.POST)
//	public String saveNewReparation(Model model,@ModelAttribute ("reparation") Reparation rep) {
//		System.out.println("#########################################");
//		service.getAll();
//		System.out.println(rep.getPanne());
//		service.save(rep);
//		return "redirect:/reparation/tousreparations";
//	}
//	
	
	//Gestion de la modification des pannes
	@PostMapping("/reparation/reparation-panne/")
	public String editReparation(Model model,@ModelAttribute("reparation") Reparation reparation) {
		model.addAttribute("pannelist", panneService.getAllPanne());
		service.getAll();
		service.update(reparation);
		successfulUpdate = true;
		return "redirect:/reparation/tousreparations";
	}
	
	@RequestMapping("/reparation/editReparation/{id}")
	public String editionReparation(Model model, @PathVariable("id") int id) {
		model.addAttribute("pannelist", panneService.getAllPanne());
		model.addAttribute("reparation", service.getById(id));
		return "ReparationTemplates/editReparation";
	}
	
	//Gestion de la suppression de panne
	@RequestMapping("/reparation/deleteReparation/{id}")
	public String deleteReparation(@PathVariable("id") int id) {
		service.deleteById(id);
		return "redirect:/reparation/tousreparations";
	}
	

}
