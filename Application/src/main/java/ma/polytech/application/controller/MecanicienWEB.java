package ma.polytech.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ma.polytech.application.entity.Mecanicien;
import ma.polytech.application.service.MecanicienService;

@Controller
public class MecanicienWEB {
	
	@Autowired
	private MecanicienService mecanicienService;
	
	@RequestMapping("/mecanicien")
	public String formMecanicien(Model model) {
		model.addAttribute("employe", new Mecanicien());
		return "EmployeTemplates/nouveau_mecanicien";
	}
	
	@PostMapping("saveMecanicien")
	public String saveMecancien(@ModelAttribute("employe") Mecanicien meca) {
		mecanicienService.save(meca);
		return "EmployeTemplates/nouveau_mecanicien";
	}

}
