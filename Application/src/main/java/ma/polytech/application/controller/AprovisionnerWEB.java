package ma.polytech.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import ma.polytech.application.entity.Approvisionner;
import ma.polytech.application.repository.EnginRepository;
import ma.polytech.application.service.ApprovisionnerService;

@Controller
public class AprovisionnerWEB {
	
	@Autowired
	private ApprovisionnerService service;
	
	@Autowired
	private EnginRepository engrepo;
	
	
	@RequestMapping("/approv/tousapprovs")
	public String findAllApprov(Model model) {
		List<Approvisionner> appros = service.getAllApprovisionnement();
		model.addAttribute("appro", appros);
		return "ApprovTempletes/allApprov";
	}
	
	

	@RequestMapping("/approv/nouveau")
	public String prepareNew(Model model) {
		Approvisionner approvs = new Approvisionner();
		model.addAttribute("enginlist", engrepo.findAll());
		model.addAttribute("appro", approvs);
		return "ApprovTempletes/nouvApprov";
	}
	
		@RequestMapping("/approv/detailApprov/{id}")
		public String detailApprov(Model model, @PathVariable("id") int id) {
			  model.addAttribute("enginlist", engrepo.findAll());
			  model.addAttribute("approv", service.getByIdApprovisionner(id));
			
			return "ApprovTempletes/detailApprov";
		}
		
	
	
	@RequestMapping("/approv/save-approv")
	public String saveNewAppro(@ModelAttribute ("appro") Approvisionner appro) {
		service.saveApprovionnement(appro);
		return "redirect:/approv/tousapprov";
	}
	
	@RequestMapping("/approv/editApprov/{id}")
	public String editApprov(Model model, @PathVariable("id") int id) {
		model.addAttribute("enginlist", engrepo.findAll());
		model.addAttribute("approvisionner", service.getByIdApprovisionner(id));
		return "ApprovTempletes/editApprov";
	}
	
	@RequestMapping("/approv/deleteApprov/{id}")
	public String deleteApprov(@PathVariable("id") int id) {
		service.deleteApprov(id);
		return "Redirect: /";
	}
	

}
