package ma.polytech.application.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ma.polytech.application.component.CurrentUser;
import ma.polytech.application.entity.Chantier;
import ma.polytech.application.entity.Engin;
import ma.polytech.application.entity.Panne;
import ma.polytech.application.entity.Reparation;
import ma.polytech.application.repository.MecanicienRepository;
import ma.polytech.application.service.EmployeService;
import ma.polytech.application.service.EnginService;
import ma.polytech.application.service.PanneService;
import ma.polytech.application.service.ReparationService;
import ma.polytech.application.service.UserServiceImp;

@Controller
public class PanneWeb {
	
	@Autowired
	private EnginService engRepo;
	
	@Autowired
	private ReparationService repaRepo;
	
	@Autowired
	private PanneService panneService;
	
	@Autowired
	private EmployeService employeService;
	
	@Autowired
	private UserServiceImp userService;
	
	@Autowired
	private MecanicienRepository mecano;
		
	private boolean successfulUpdate = false;

	private boolean successfulCreate = false;
	
	
	@RequestMapping("/panne/touspannes")
	public String findAllpannes(Model model) {
		CurrentUser currentUser = new CurrentUser(employeService, userService);
		model.addAttribute("currentUser", currentUser.getCurrentUser());
		model.addAttribute("pannes", panneService.getAllPanne());
		model.addAttribute("reparationlist", repaRepo.getAll());
		model.addAttribute("enginlist", engRepo.getAll());
		model.addAttribute("newPanne", new Panne());
		model.addAttribute("successfulUpdate", successfulUpdate);
		model.addAttribute("successfulCreate", successfulCreate);
		successfulUpdate = false;
		successfulCreate = false;
		return "PanneTemplates/allPanne";
	}
	
//	@RequestMapping("/panne/newpanne")
//	public String prepareNewpanne(Model model) {
//		Panne panne = new Panne();
//		model.addAttribute("reparationlist", repaRepo.findAll());
//		model.addAttribute("enginlist", engRepo.findAll());
//		model.addAttribute("panne", panne);
//		return "PanneTemplates/dash-nou-panne";
//	}
	
	@RequestMapping(value= "/panne/savepanne", method = RequestMethod.POST)
	public String savepanne(Model model, @ModelAttribute ("newPanne") Panne panne) {
		model.addAttribute("reparationlist", repaRepo.getAll());
		model.addAttribute("enginlist", engRepo.getAll());
		model.addAttribute("newPanne", new Panne());
		panne.setReparation(null);
		panneService.savePanne(panne);
		successfulCreate = true;
		return "redirect:/panne/touspannes";
	}
	
	@PostMapping("/panne/modificationPanne/")
	public String editionpanne(@ModelAttribute("panne") Panne panne) {
		System.out.println("############################");
		System.out.println(panne.getIdPanne());
		System.out.println("############################");
		panneService.updatePanne(panne);
		successfulUpdate = true;
		return "redirect:/panne/touspannes";
	}
	
//	@PostMapping("/panne/modifPanne/")
//	public String editepanne(@ModelAttribute("panne") Panne panne, @RequestParam("selectEngin") int tab[]) {
//		
//		Set<Reparation> reparations= new HashSet<Reparation>();
//		for(int i: tab) {
//			reparations.add(repaRepo.findById(i).orElse(null));
//		}
//		panne.setReparation(reparations);
//		service.updatePanne(panne);
//		return "redirect:/panne/touspannes";
//	}
	
	@RequestMapping("/panne/editPanne/{id}")
	public String profilPanne(Model model, @PathVariable("id") int id) {
		model.addAttribute("panne", panneService.getByIdPanne(id));
		model.addAttribute("reparationlist", repaRepo.getAll());
		model.addAttribute("enginlist", engRepo.getAll());
		model.addAttribute("newPanne", new Panne());
		return "/PanneTemplates/modif";
	}
	
	@RequestMapping("/reparation/nouveauReparation/{id}")
	public String nouveauReparation(Model model, @PathVariable("id") int id) {
		Panne panneToRepare = panneService.getByIdPanne(id);
		Set<Panne> pannelist = new HashSet<Panne>();
		pannelist.add(panneToRepare);
		model.addAttribute("pannelist", pannelist);
		model.addAttribute("mecanicienlist", mecano.findAll());
		model.addAttribute("reparation", new Reparation());
		return "ReparationTemplates/nouveauReparation";
	}
	
	@RequestMapping(value="/reparation/saveReparation/", method = RequestMethod.POST)
	public String saveNewReparation(Model model, @ModelAttribute("reparation") Reparation reparation ) {
		Panne panneToRepare = reparation.getPanne();
		panneToRepare.setReparation(reparation);
		repaRepo.save(reparation);
		panneService.updatePanne(panneToRepare);
		return "redirect:/reparation/tousreparations";
	}
	
	
	@RequestMapping("/panne/deletePanne/{id}")
	public String deletepanne(Model model,@PathVariable("id") int id) {
		panneService.deletePanne(id);
		return "redirect:/panne/touspannes";
	}
	
}
