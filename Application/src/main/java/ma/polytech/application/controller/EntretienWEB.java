package ma.polytech.application.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ma.polytech.application.component.CurrentUser;
import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.Engin;
import ma.polytech.application.entity.Entretien;
import ma.polytech.application.entity.Mecanicien;
import ma.polytech.application.repository.MecanicienRepository;
import ma.polytech.application.service.EmployeService;
import ma.polytech.application.service.EnginService;
import ma.polytech.application.service.EntretienService;
import ma.polytech.application.service.MecanicienService;
import ma.polytech.application.service.UserServiceImp;
import ma.polytech.application.validator.EntretienUpdateValidator;
import ma.polytech.application.validator.EntretienValidator;


@Controller
public class EntretienWEB {
	@Autowired
	private EntretienService service;
	@Autowired
	private EnginService engService;
	
	@Autowired
	private EmployeService employeService;
	
	@Autowired
	private UserServiceImp userService;
	
	@Autowired
	private EntretienValidator entretienValidator;
	
	@Autowired
	private EntretienUpdateValidator entretienUpdateValidator;
	
	@Autowired
	private MecanicienService mecanicienService;
	
	@Autowired
	private MecanicienRepository mecaRepo;
		
	private boolean successfulUpdate = false;

	private boolean successfulCreate = false;
	
	@RequestMapping("/entretien/tousentretiens")
	public String saveEntretien(Model model) {
		List<Entretien> entretiens = service.getAllEntretien();
		CurrentUser currentUser = new CurrentUser(employeService, userService);
		model.addAttribute("currentUser", currentUser.getCurrentUser());
		model.addAttribute("entretiens", entretiens);
		model.addAttribute("mecanicienList", mecanicienService.getAll());
		model.addAttribute("enginList", engService.getAll());
		model.addAttribute("newEntretien", new Entretien());
		model.addAttribute("successfulUpdate", successfulUpdate);
		model.addAttribute("successfulCreate", successfulCreate);
		successfulUpdate = false;
		successfulCreate = false;
		return "EntretienTemplates/allEntretien";
	}
	
	@RequestMapping("/entretien/new-entretien")
	public String prepareNewEntretien(Model model) {
		model.addAttribute("mecanicienList", mecanicienService.getAll());
		System.out.println(mecanicienService.getAll());
		model.addAttribute("enginList", engService.getAll());
		model.addAttribute("newEntretien", new Entretien());
		return "EntretienTemplates/dash-nouv-entretien";

	}
	
	@RequestMapping(value = "/entretien/save-entretien", method = RequestMethod.POST)
	public String saveEntretien(Model model, @ModelAttribute ("newEntretien") Entretien entretien,
			BindingResult bindingResult,
			@RequestParam(value = "mecaniciens", required = false) int[] mecaniciens) {
		entretienValidator.validate(entretien, bindingResult);
		System.out.println("####################### ENTRETIEN ################");
		System.out.println(entretien);
		System.out.println("#######################################");
		if(bindingResult.hasErrors()) {
			model.addAttribute("showModalEntretien", true);
			if(bindingResult.hasFieldErrors("date"))
				model.addAttribute("dateError", "error in date");
			if(bindingResult.hasFieldErrors("titre"))
				model.addAttribute("titreError", "error in titre");
			CurrentUser currentUser = new CurrentUser(employeService, userService);
			model.addAttribute("currentUser", currentUser.getCurrentUser());
			model.addAttribute("entretiens", service.getAllEntretien());
			model.addAttribute("mecanicienList", mecanicienService.getAll());
			model.addAttribute("enginList", engService.getAll());
			model.addAttribute("newEntretien", new Entretien());
			return "EntretienTemplates/allEntretien";
		}
		service.saveEntretien(entretien);
		successfulCreate = true;
		return "redirect:/entretien/tousentretiens";
	}

	@RequestMapping(value = "/entretien/save-edit-entretien", method = RequestMethod.POST)
	public String saveEditEntretien(Model model, @ModelAttribute ("entretien") Entretien entretien,
			BindingResult bindingResult,
			@RequestParam(value = "mecaniciens", required = false) int[] mecaniciens) {
		entretienUpdateValidator.validate(entretien, bindingResult);
		if(bindingResult.hasErrors()) {
			if(bindingResult.hasFieldErrors("date"))
				model.addAttribute("dateError", "error in date");
			if(bindingResult.hasFieldErrors("titre"))
				model.addAttribute("titreError", "error in titre");
			CurrentUser currentUser = new CurrentUser(employeService, userService);
			model.addAttribute("currentUser", currentUser.getCurrentUser());
			model.addAttribute("mecanicienList", mecanicienService.getAll());
			model.addAttribute("enginList", engService.getAll());
			model.addAttribute("newEntretien", new Entretien());
			model.addAttribute("entretien", entretien);
			return "EntretienTemplates/editEntretien";
		}
		service.updateEntretien(entretien);
		successfulUpdate = true;
		model.addAttribute("updateOK", "success");
		return "redirect:/entretien/tousentretiens";
	}
	
	@RequestMapping("entretien/editEntretien/{id}")
	public String editeEntretient(Model model, @PathVariable("id") int id) {
		CurrentUser currentUser = new CurrentUser(employeService, userService);
		model.addAttribute("currentUser", currentUser.getCurrentUser());
		model.addAttribute("enginList", engService.getAll());
		model.addAttribute("mecanicienList", mecanicienService.getAll());
		model.addAttribute("newEntretien", new Entretien());
		model.addAttribute("entretien", service.getById(id));
		return "EntretienTemplates/editEntretien";
	}
	
	@RequestMapping("/entretien/delete/{id}")
	public String deleteEntretien(@PathVariable("id") int id) {
		service.deleteEntretien(id);
		return "redirect:/entretien/tousentretiens";
	}
}
