package ma.polytech.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.Poste;
import ma.polytech.application.service.EmployeService;
import ma.polytech.application.service.PosteService;

@Controller
public class PostWeb {
	
	@Autowired
	private PosteService posteService;
	
	@Autowired
	private EmployeService employeService;
		
	private boolean successfulUpdate = false;

	private boolean successfulCreate = false;
	
	@RequestMapping("/poste")
	public String allPoste(Model model) {
		List<Poste> postes = posteService.getAllPoste();
		model.addAttribute("postes", postes);
		return "PosteTemplates/liste-poste";
	}
	
	@RequestMapping("/poste/liste-poste")
	public String listPoste(Model model) {
		List<Poste> listp = posteService.getAllPoste();
		model.addAttribute("postes", listp);
		model.addAttribute("successfulUpdate", successfulUpdate);
		model.addAttribute("successfulCreate", successfulCreate);
		successfulUpdate = false;
		successfulCreate = false;
		return "PosteTemplates/liste-poste";
	}
	
	@RequestMapping("/poste/creer-nouveau-poste")
	public String createPoste(Model model) {
		List<Poste> postes = posteService.getAllPoste();
		Poste newPoste = new Poste();
		model.addAttribute("newPoste", newPoste);
		model.addAttribute("postes", postes);
		return "dashboard_test";
	}
	
	@RequestMapping("/poste/delete/{id}")
	public String supprimerPoste(Model model, @PathVariable("id") int id) {
		posteService.deleteByid(id);
		return "redirect:/employe/dashboard-employe";
	}
	
	@RequestMapping("/poste/{id}-members")
	public String employesByposte(Model model, @PathVariable("id") int id) {
		/* ################################## */
		Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
		String email = loggedInUser.getName();
		Employe empLogged = employeService.findByEmail(email);
		model.addAttribute("currentUser", empLogged);
		/* ################################## */

		model.addAttribute("newPoste", new Poste());
		model.addAttribute("newEmploye", new Employe());
		model.addAttribute("postes", posteService.getAllPoste());
		model.addAttribute("employes", employeService.findByPoste(posteService.getByIdPoste(id)));
		return "EmployeTemplates/dashboard-employe";
	}

	@RequestMapping("/poste/save-poste")
	public String savePoste(@ModelAttribute("newPoste") Poste newPoste) {
		posteService.savePoste(newPoste);
		successfulCreate = true;
		return "redirect:/employe/dashboard-employe";
	}

	@PostMapping("/poste/edit")
	public String editPoste(@ModelAttribute("newPoste") Poste newPoste) {
		if(posteService.findPosteByNom(newPoste.getOldNom()) != null) {
			Poste postEdit = posteService.findPosteByNom(newPoste.getOldNom());
			postEdit.setNom(newPoste.getNom());
			posteService.savePoste(postEdit);
		}
		successfulUpdate = true;
		return "redirect:/employe/dashboard-employe";
	}
	
}
