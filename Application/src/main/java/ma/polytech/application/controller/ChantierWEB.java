package ma.polytech.application.controller;



import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import ma.polytech.application.component.CurrentUser;
import ma.polytech.application.entity.Chantier;
import ma.polytech.application.entity.Engin;
import ma.polytech.application.repository.ChantierRepository;
import ma.polytech.application.repository.EnginRepository;
import ma.polytech.application.repository.ResponsableRepository;
import ma.polytech.application.service.ChantierService;
import ma.polytech.application.service.EmployeService;
import ma.polytech.application.service.EnginService;
import ma.polytech.application.service.ResponsableService;
import ma.polytech.application.service.UserServiceImp;
import ma.polytech.application.validator.ChantierValidate;

@Controller
public class ChantierWEB {
	@Autowired
	private ChantierService service;

	@Autowired
	private EnginService enginService;
	
	@Autowired
	private ChantierService chantierService;
	
	@Autowired
	private ResponsableService responsableService;
	
	@Autowired
	private EmployeService employeService;
	
	@Autowired
	private ChantierValidate chantierValidation;
	
	@Autowired
	private UserServiceImp userService;
	
	private boolean successfulUpdate = false;

	private boolean successfulCreate = false;
	
	private boolean modalShow = false;
	private boolean nomError = false;
	private boolean dateError = false;
	
	@RequestMapping("/chantier/touschantiers")
	public String findAllChantiers(Model model) {
		CurrentUser currentUser = new CurrentUser(employeService, userService);
		model.addAttribute("currentUser", currentUser.getCurrentUser());
		List<Chantier> chantiers = service.getAllChantier();
		model.addAttribute("chantier", new Chantier());
		model.addAttribute("responsables", responsableService.getAll());
		model.addAttribute("enginlist", enginService.getAll());
		model.addAttribute("chantiers", chantiers);
		model.addAttribute("successfulUpdate", successfulUpdate);
		model.addAttribute("successfulCreate", successfulCreate);
		model.addAttribute("modalShow", modalShow);
		model.addAttribute("nom", nomError);
		model.addAttribute("date", dateError);
		successfulUpdate = false;
		successfulCreate = false;
		modalShow = false;
		nomError = false;
		dateError = false;
		return "ChantierTemplates/listChantier";
	}
	
	@RequestMapping("/chantier/t/")
	public String findAllChantier(Model model) {
		Chantier chantier = new Chantier();
		model.addAttribute("chantier", chantier);
		model.addAttribute("responsables", responsableService.getAll());
		model.addAttribute("enginlist", enginService.getAll());
		
		return "ChantierTemplates/newChantier";
	}
	
	@RequestMapping(value="/save-chantier", method = RequestMethod.POST)
	public String saveChantier(Model model, @ModelAttribute ("chantier") Chantier chantier,
			BindingResult bindingResult) {
		chantierValidation.validate(chantier, bindingResult);
		if(bindingResult.hasErrors()) {
			if(bindingResult.hasFieldErrors("nom")) {
				nomError = true;
			}
			if(bindingResult.hasFieldErrors("DateDebut")) {
				dateError = true;
			}
			modalShow = true;
			return "redirect:/chantier/touschantiers";
		}
		service.saveChantier(chantier);
		successfulCreate = true;
		return "redirect:/chantier/touschantiers";
	}
	
	@PostMapping("/chantier/editChantier/")
	public String editChantier(@ModelAttribute("chantier") Chantier chantier,
			@RequestParam(value = "responsablesSelected", required = false) int[] responsablesId,
			@RequestParam(value = "enginsSelected", required = false) int[] enginsId,
			@RequestParam("file") MultipartFile file) {
		/* ADD ENGIN SELECTED TO CHANTIER */
		if(enginsId != null)
		for (int i : enginsId) {
			chantier.getEngin().add(enginService.getById(i));
		}
		/* ADD RESPONSABLE SELECTED TO CHANTIER */
		if(responsablesId != null)
		for (int i : responsablesId) {
			chantier.getResponsable().add(responsableService.getById(i));
		}
		service.updateChantier(chantier);
		successfulUpdate = true;
		uploadToLocalFileSystem(file, chantier.getIdChantier());
		return "redirect:/chantier/touschantiers";
	}
	
	@GetMapping("/chantier/editChantier/{id}")
	public String editSuivantChantier(Model model, @PathVariable("id") int id) {
		CurrentUser currentUser = new CurrentUser(employeService, userService);
		model.addAttribute("currentUser", currentUser.getCurrentUser());
		model.addAttribute("responsableList", responsableService.getAll());
		model.addAttribute("enginList", enginService.getAll());
		model.addAttribute("chantier", service.getByIdChantier(id));
		return "ChantierTemplates/changeChantier";
	}
	
	@RequestMapping("/chantier/deleteChantier/{id}")
	public String deleteChantier(Model model,@PathVariable("id") int id) {
		service.deleteChant(id);
		return "redirect:/chantier/touschantiers";
	}
	
	public void uploadToLocalFileSystem(MultipartFile file, int id) {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		Path path = Paths.get("src/main/resources/static/img/chantier/" + id + ".jpg");
		try {
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
//		return "upload_file";
		System.out.println("####################### SAVED FILE ####################");
	}
}
 