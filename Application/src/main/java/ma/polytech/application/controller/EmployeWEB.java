package ma.polytech.application.controller;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javassist.expr.NewArray;
import ma.polytech.application.configuration.SecurityService;
import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.Poste;
import ma.polytech.application.entity.User;
import ma.polytech.application.repository.PosteRepository;
import ma.polytech.application.repository.UserRepository;
import ma.polytech.application.service.EmployeService;
import ma.polytech.application.service.PosteService;
import ma.polytech.application.service.UserServiceImp;
import ma.polytech.application.validator.EmployeValidator;
import ma.polytech.application.validator.UserValidator;

@Controller
public class EmployeWEB {

	// JUSTE POUR TESTER GITHUB COLLABORATION

	@Autowired
	private EmployeService employeService;

	@Autowired
	private PosteService posteService;

	@Autowired
	private EmployeValidator employeValidator;
	
	@Autowired
	private UserServiceImp userService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PosteRepository posteRepository;
		
	private boolean successfulUpdate = false;

	private boolean successfulCreate = false;

	@RequestMapping("/dashboard")
	public String allEmployes(Model model) {

		/* ################################## */
//		User currentUser = currentUser();
		model.addAttribute("currentUser", currentUser());
		/* ################################## */

		model.addAttribute("employes", employeService.getAll());
		model.addAttribute("postes", posteService.getAllPoste());
		return "dashboard";
	}

	@RequestMapping("/employe/dashboard-employe")
	public String dashboard(Model model) {

		/* ################################## */
//		User currentUser = currentUser();
		model.addAttribute("currentUser", currentUser());
		/* ################################## */

		model.addAttribute("newPoste", new Poste());
		model.addAttribute("newEmploye", new Employe());
		model.addAttribute("employes", employeService.getAll());
		model.addAttribute("postes", posteService.getAllPoste());
		model.addAttribute("successfulUpdate", successfulUpdate);
		model.addAttribute("successfulCreate", successfulCreate);
		successfulUpdate = false;
		successfulCreate = false;
		return "EmployeTemplates/dashboard-employe";
	}


	/*
	 * @RequestMapping("/employe/profil") public String profil(Model model, int id)
	 * {
	 * 
	 * ################################## Employe currentUser = currentUser();
	 * model.addAttribute("currentUser", currentUser);
	 * ##################################
	 * 
	 * Employe emp = employeService.getById(id); model.addAttribute("employe", emp);
	 * return "EmployeTemplates/profil"; }
	 */

	@PostMapping("/employe/save-employe")
	public String saveEmploye(Model model, @ModelAttribute("newEmploye") Employe employe,
			@RequestParam("file") MultipartFile file, @ModelAttribute("postes") Employe postes,
			BindingResult bindingResult) {
		employeValidator.validate(employe, bindingResult);

		/* ################################## */
//		User currentUser = currentUser();
		model.addAttribute("currentUser", currentUser());
		/* ################################## */

		System.out.println("################### EMPLOYE SAVE ##################");
		System.out.println(employe.getDate());
		if (bindingResult.hasErrors()) {
			if (bindingResult.hasFieldErrors("username"))
				model.addAttribute("username", "error username");
			if (bindingResult.hasFieldErrors("email"))
				model.addAttribute("email", "error email");
			return "redirect:/employe/submit-error";
		}

		employeService.save(employe, bindingResult);
		uploadToLocalFileSystem(file, employe.getEmail());
		successfulCreate = true;
		System.out.println("################### EMPLOYE SAVED OK ##################");
		return "redirect:/employe/dashboard-employe";
	}
	
	@RequestMapping("/employe/submit-error")
	public String submitError(Model model, @ModelAttribute("email") String email, @ModelAttribute("username") String username) {
		model.addAttribute("postes", posteService.getAllPoste());
		model.addAttribute("employes", employeService.getAll());

		model.addAttribute("newPoste", new Poste());
		model.addAttribute("newEmploye", new Employe());
		
		model.addAttribute("modalShow", true);
		model.addAttribute("email", email);
		model.addAttribute("username", username);

		model.addAttribute("currentUser", currentUser());
		return "EmployeTemplates/dashboard-employe";
	}

	@PostMapping("/employe/save-edit-employe")
	public String saveUpdateEmploye(Model model, @ModelAttribute("employe") Employe employe,
			@RequestParam("file") MultipartFile file, @ModelAttribute("postes") Employe postes,
			BindingResult bindingResult) {
		System.out.println("################### EMPLOYE UPDATE SAVE ##################");
		System.out.println(employe.getUsername());
		System.out.println(employe.getPrenom());
		System.out.println(employe.getNom());
		System.out.println(employe.getEmail());
		System.out.println(employe.getPassword());
		employeValidator.validateUpdate(employe, bindingResult);

		/* ################################## */
//		User currentUser = currentUser();
		model.addAttribute("currentUser", currentUser());
		/* ################################## */

		System.out.println("###################EMPLOYE SAVE##################");
//		System.out.println(employe.getDate());
		if (bindingResult.hasErrors()) {
			if (bindingResult.hasFieldErrors("username"))
				model.addAttribute("username", "error username");
			if (bindingResult.hasFieldErrors("email"))
				model.addAttribute("email", "error email");
			model.addAttribute("postes", postes);
			model.addAttribute("employe", employe);
			model.addAttribute("newEmploye", new Employe());
			model.addAttribute("newPoste", new Poste());
			model.addAttribute("modalShow", false);
			return "redirect:/employe/profile/" + employe.getId();
		}

		employeService.update(employe, bindingResult);
		uploadToLocalFileSystem(file, employe.getEmail());
		successfulUpdate = true;
		return "redirect:/employe/dashboard-employe";
	}

	/*
	 * @RequestMapping("/employe/edit/{id}") private String editEmploye(Model
	 * model, @PathVariable("id") int id) { List<Poste> postes =
	 * posteService.getAllPoste(); model.addAttribute("employe",
	 * employeService.getById(id)); model.addAttribute("postes", postes); return
	 * "EmployeTemplates/update_employe"; }
	 */

	@RequestMapping("/employe/delete/{id}")
	private String deleteEmploye(Model model, @ModelAttribute("currentUser") User currentUser, @PathVariable("id") int id) {

		/* DELETE USER SIGN IN ATTCHED TO EMPLOYE THAT WILL BE DELETE */
		String employeEmail = employeService.getById(id).getEmail();
		System.out.println(employeEmail);
		if(userService.findByEmail(employeEmail) != null) {
			userService.delete(userService.findByEmail(employeEmail));
		}

		/* DELETE EMPLOYE */
		employeService.deleteById(id);
		
		if(currentUser().getId() == id)
			return "redirect:/logout";

		return "redirect:/employe/dashboard-employe";
	}

	@RequestMapping("/employe/profile/{id}")
	private String profile(Model model, @PathVariable("id") int id) {

		/* ################################## */
		model.addAttribute("currentUser", currentUser());
		/* ################################## */

		model.addAttribute("newPoste", new Poste());
		model.addAttribute("newEmploye", new Employe());
		model.addAttribute("postes", posteService.getAllPoste());
		model.addAttribute("employe", employeService.getById(id));
		return "EmployeTemplates/profile";
	}

//	@PostMapping("/employe/upload")
	public void uploadToLocalFileSystem(MultipartFile file, String email) {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		Path path = Paths.get("src/main/resources/static/img/user/" + email + ".jpg");
		try {
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
//		return "upload_file";
	}

	public Employe currentUser() {
		Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
		String email = loggedInUser.getName();
		Employe empLogged = employeService.findByEmail(email);
		if(empLogged == null)
			return createTestEmploye();
		return empLogged; // this is the current logged user
	}
		
//	@EventListener(ApplicationReadyEvent.class)
	public Employe createTestEmploye() {
		Employe employeTest = employeService.findByUsername("default");
		if(employeTest == null) {
			return employeService.save(new Employe("default", "default", "default", "default@gmail.com", "default", new Date(), new BigInteger("76789292")));
		}

		/* CREATE DEFAULT EMPLOYE ADMIN */
		if(employeService.findByUsername("default_admin") == null || employeService.findByEmail("yattdeveloper@gmail.com") == null) {
			Employe adminNew = new Employe("default_admin", "default_admin", "default_admin", "yattdeveloper@gmail.com", "76789292", new Date(), new BigInteger("76789292"));
			if(posteRepository.findByNom("Responsable") == null)
				posteRepository.save(new Poste("Responsable"));
			adminNew.setPoste(posteRepository.findByNom("Responsable"));
			employeService.save(adminNew);
		} else if (!employeService.findByUsername("default_admin").postNom()
				.contentEquals("Responsable")) { /* UPDATE ADMIN POSTE IF CHANGED */
			if(posteRepository.findByNom("Responsable") == null)
				posteRepository.save(new Poste("Responsable"));
			employeService.findByUsername("default_admin").setPoste(posteRepository.findByNom("Responsable"));
			BindingResult bindingResult = null;
			employeService.update(employeService.findByUsername("default_admin"), bindingResult);
		}
		
		/* CREATE DEFAULT USER ADMIN */
		if(userService.findByUsername("default_admin") == null || userService.findByEmail("yattdeveloper@gmail.com") == null) {
			User user = new User("default_admin", "yattdeveloper@gmail.com", "76789292");
			userService.save(user);
		}
		return employeTest;
	}

}
