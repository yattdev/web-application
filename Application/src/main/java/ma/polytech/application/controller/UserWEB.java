package ma.polytech.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ma.polytech.application.configuration.SecurityService;
import ma.polytech.application.configuration.SecurityServiceImpl;
import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.User;
import ma.polytech.application.service.UserService;
import ma.polytech.application.service.UserServiceImp;
import ma.polytech.application.validator.UserValidator;

@Controller
public class UserWEB {

	@Autowired
	private UserServiceImp userService;

	@Autowired
	private UserValidator userValidator;
	
    @Autowired
    private SecurityServiceImpl securityService;
    
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
	
    @GetMapping("/inscription")
    public String inscription(Model model) {
        model.addAttribute("user", new User());

        return "inscription";
    }

    @PostMapping("/inscription")
    public String inscription(Model model, @ModelAttribute("user") User userForm, BindingResult bindingResult) {
        System.out.println("############# DANS INSCRIPTION ############################");
        userValidator.validate(userForm, bindingResult);

		System.out.println("#########################################");
        System.out.println(userForm.getAgreeTerms());
        System.out.println("#########################################");
        if (bindingResult.hasErrors()) {
            return "inscription";
        }

		System.out.println("#########################################");
        System.out.println(userForm.getPassword());
        System.out.println("#########################################");       
        userService.save(userForm);
//        securityService.autoLogin(userForm.getUsername(), userForm.getPassword());

        return "redirect:/employe/dashboard-employe";
    }

    @GetMapping({"/", "/login"})
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Username ou Password invalide.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
        
        return "login";
    }
    
    @RequestMapping(value = {"/logout", "/logoutSuccessful"}, method = RequestMethod.GET)
    public String logoutSuccessfulPage(Model model) {
        model.addAttribute("logout", new String("You have been log out successfully."));
        return "login";
    }

//    @PostMapping("/login")
//    public String login(Model model, String error, String logout) {
//        if (error != null)
//            model.addAttribute("error", "Your username and password is invalid.");
//
//        if (logout != null)
//            model.addAttribute("message", "You have been logged out successfully.");
//
//        return "login";
//    }
    
	@RequestMapping({"/mot-de-passe-oublie"})
	public String passeOublier(Model model, @ModelAttribute("email") String email) {
		User existUser = userService.findByEmail(email);
		if(existUser == null) {
			model.addAttribute("error", "Your email is invalid.");
			return "login";
		}
		model.addAttribute("resetpass", "Vefifier votre email s'il vous plait ! Un passe de recuperation vous es envoye");
		return "login";
	}
    
}
