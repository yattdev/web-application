package ma.polytech.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ma.polytech.application.entity.Employe;
import ma.polytech.application.service.EmployeService;

@RestController
public class EmployeController {

	@Autowired
	private EmployeService service;
	
	@GetMapping(path = "/employes/", produces = "application/json ; chartset=UTF-8")
	public List<Employe> getAll(){
		return service.getAll();
	}
	
	@RequestMapping(path = "/employe/{id}", produces = "application/json ; chartset=UTF-8")
	@ResponseBody
	public Employe getById(@PathVariable int id) {
		return service.getById(id);
	}
	
//	@PostMapping(path = "/addEmploye", produces = "application/json ; chartset=UTF-8")
//	public Employe add(@RequestBody Employe emp) {
//		return service.save(emp);
//	}
	
	//@RequestMapping(path = "/addEmploye", produces = "application/json ; chartset=UTF-8")

	
}
