package ma.polytech.application.controller;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ma.polytech.application.component.CurrentUser;
import ma.polytech.application.entity.Chantier;
import ma.polytech.application.entity.Engin;
import ma.polytech.application.service.ApprovisionnerService;
import ma.polytech.application.service.ChantierService;
import ma.polytech.application.service.ConducteurService;
import ma.polytech.application.service.EmployeService;
import ma.polytech.application.service.EnginService;
import ma.polytech.application.service.EntretienService;
import ma.polytech.application.service.PanneService;
import ma.polytech.application.service.UserServiceImp;

@Controller
public class EnginWEB {
	@Autowired
	private EnginService service;

	@Autowired
	private ApprovisionnerService approvisionnerService;
	
	@Autowired
	private PanneService panneService;

	@Autowired
	private ChantierService chantierService;

	@Autowired
	private ConducteurService conducteurService;

	@Autowired
	private EntretienService entretienService;
	
	@Autowired
	private EmployeService employeService;
	
	@Autowired
	private UserServiceImp userService;
		
	private boolean successfulUpdate = false;

	private boolean successfulCreate = false;
	
	@RequestMapping("/engin/tousengins")
	public String findAllEngins(Model model) {
		CurrentUser currentUser = new CurrentUser(employeService, userService);
		model.addAttribute("engins", service.getAll());
		model.addAttribute("currentUser", currentUser.getCurrentUser());
		model.addAttribute("newEngin", new Engin());
		model.addAttribute("successfulUpdate", successfulUpdate);
		model.addAttribute("successfulCreate", successfulCreate);
		successfulUpdate = false;
		successfulCreate = false;
		return "EnginTemplates/allEngin";
	}
	
	@RequestMapping("/engin")
	public String findAllEngin(Model model) {
		List<Engin> engins = service.getAll();
		model.addAttribute("engins", engins);
		return "EnginTemplates/nouvauEngin";
	}
	
	
	@RequestMapping("/engin/next-engin")
	public String prepareSuivantEngin(Model model, @ModelAttribute("engin") Engin engin) {
		  model.addAttribute("chantierlist", chantierService.getAllChantier());
		  model.addAttribute("pannelist", panneService.getAllPanne());
		  model.addAttribute("conducteurlist", conducteurService.getAll());
		  model.addAttribute("entretienlist", entretienService.getAllEntretien());
		  model.addAttribute("approvlist", approvisionnerService.getAllApprovisionnement());
		  model.addAttribute("engin", engin);
		  return "EnginTemplates/finalEngin";
	}
	
	@RequestMapping(value="/engin/save-engin", method = RequestMethod.POST)
	public String saveNewEngin(@ModelAttribute("newEngin") Engin engin) {
		service.saveEngin(engin);
		successfulCreate = true;
		System.out.println("################### SAVED ###################");
		return "redirect:/engin/tousengins";
	}
	
	@RequestMapping("/engin/editEngin/{id}")
	public String editEngin(Model model, @PathVariable ("id") int id) {
		CurrentUser currentUser = new CurrentUser(employeService, userService);
		model.addAttribute("currentUser", currentUser.getCurrentUser());
		model.addAttribute("chantierList", chantierService.getAllChantier());
		model.addAttribute("panneList", panneService.getAllPanne());
		model.addAttribute("conducteurList", conducteurService.getAll());
		model.addAttribute("entretienList", entretienService.getAllEntretien());
//		model.addAttribute("approvlist", approvisionnerService.getAllApprovisionnement());
		model.addAttribute("engin", service.getById(id));
		model.addAttribute("newEngin", new Engin());
		return "EnginTemplates/edit_engin";
	}
	
	@RequestMapping(value="/engin/save-edit-engin", method = RequestMethod.POST)
	public String saveEditEngin(@ModelAttribute("engin") Engin engin, 
//			@RequestParam(value = "pannesSelected" , required = false) int[] pannes,
//			@RequestParam(value = "chantiersSelected" , required = false) int[] chantiers,
//			@RequestParam(value = "entretiensSelected" , required = false) int[] entretiens,
			@RequestParam(value = "conducteursSelected" , required = false) int[] conducteurs) {

		/* ERASE CONTENT */
		engin.getConducteur().clear();
		if(conducteurs != null)
		for(int i: conducteurs) {
			engin.getConducteur().add(conducteurService.getById(i));
		}

		service.updateEngin(engin);
		System.out.println("################### SAVED ###################");
		System.out.println(engin);
		successfulUpdate = true;
		return "redirect:/engin/tousengins";
	}

	@RequestMapping("/engin/delete/{id}")
	public String deleteEngin(@PathVariable ("id") int id) {
		service.deleteEngin(id);
		return "redirect:/engin/tousengins";
	}
}
