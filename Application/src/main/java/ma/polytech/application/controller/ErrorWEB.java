package ma.polytech.application.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorWEB {

  @RequestMapping("/error")
  public void handleRequest() {
      throw new RuntimeException("test exception");
  }
}