package ma.polytech.application.entity;

import javax.persistence.*;
//import javax.validation.constraints.Email;
//import javax.validation.constraints.NotEmpty;

import java.util.Collection;

@Entity
@Table(name = "user", catalog = "project", uniqueConstraints = {
	@UniqueConstraint(columnNames = "username"),
	@UniqueConstraint(columnNames = "email") })
public class User{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	protected Integer idEmploye;

    protected String username;
    protected String email;
    protected String password;
    protected Boolean agreeTerms;


	@Column(name = "roles", nullable = false)
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
        name = "users_roles",
        joinColumns = @JoinColumn(
            name = "user_id", referencedColumnName = "idEmploye"),
        inverseJoinColumns = @JoinColumn(
            name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;
	
	public User(String username, String email, String password) {
		// TODO Auto-generated constructor stub
		this.username = username;
		this.email = email;
		this.password = password;
	}
	
	public User() {
		// TODO Auto-generated constructor stub
	}

	@Column(name = "idEmploye", unique = true, nullable = false)
	public Integer getId() {
		return this.idEmploye;
	}

	public void setId(Integer idEmploye) {
		this.idEmploye = idEmploye;
	}    

	@Column(name = "email", unique = true, nullable = false, length = 45)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "username", unique = true, nullable = false, length = 45)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", nullable = false, length = 45)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

    /**
	 * @return the agreeTerms
	 */
	public Boolean getAgreeTerms() {
		return agreeTerms;
	}

	/**
	 * @param agreeTerms the agreeTerms to set
	 */
	public void setAgreeTerms(Boolean agreeTerms) {
		this.agreeTerms = agreeTerms;
	}

	public Collection < Role > getRoles() {
        return roles;
    }

    public void setRoles(Collection < Role > roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "User{" +
            "id=" + idEmploye +
            ", username='" + username + '\'' +
            ", email='" + email + '\'' +
            ", password='" + "*********" + '\'' +
            ", roles=" + roles +
            '}';
    }

}
