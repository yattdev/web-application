package ma.polytech.application.entity;
// Generated 15 déc. 2019 16:12:14 by Hibernate Tools 4.3.5.Final

import java.util.Date;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import ma.polytech.application.service.ChantierService;

/**
 * Chantier generated by hbm2java
 */
@Entity
@Table(name = "Chantier", catalog = "project")
public class Chantier implements java.io.Serializable {

	private Integer idChantier;
	private Set<Employe> responsable = new HashSet<Employe>(0);
	private String nom=null;
	private String adresse=null;
	private Set<Engin> engin = new HashSet<Engin>(0);
	private String contactClient = null;
	private String pays = null;
	private String client = null;
	private String ville = null;
	private Double budget = null;
	private String description = null;
	
	  @Temporal(TemporalType.DATE)
	  @DateTimeFormat(pattern = "yyyy-MM-dd")
	  private Date DateDebut;
	  
	  @Temporal(TemporalType.DATE)
	  @DateTimeFormat(pattern = "yyyy-MM-dd") 
	  private Date Datefin;
	 
	
	public Chantier() {
	}

	public Chantier(String nom, String adresse) {
		this.nom = nom;
		this.adresse = adresse;
	}

	public Chantier(Set<Employe> responsable, String nom, String adresse, Set<Engin> engin) {
		this.responsable = responsable;
		this.nom = nom;
		this.adresse = adresse;
		this.engin = engin;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	
	@Column(name = "id_chantier", unique = true, nullable = false)
	public Integer getIdChantier() {
		return this.idChantier;
	}

	public void setIdChantier(Integer idChantier) {
		this.idChantier = idChantier;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="Personnel", catalog = "project", 
		joinColumns = { @JoinColumn(name="chantier", nullable = false, updatable = false)},
		inverseJoinColumns = {@JoinColumn(name="responsable", nullable = false, updatable = false)})
	public Set<Employe> getResponsable() {
		return this.responsable;
	}

	public void setResponsable(Set<Employe> responsable) {
		this.responsable=responsable;
	}
	
	@Column(name = "nom", nullable = true, length = 45)
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Column(name = "adresse", nullable = true, length = 45)
	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "Attribuer", catalog = "project", joinColumns = {
			@JoinColumn(name = "chantier", nullable = false, updatable = false)},
			inverseJoinColumns = {@JoinColumn(name = "engin", nullable = false, updatable = false)})
	public Set<Engin> getEngin() {
		return this.engin;
	}

	public void setEngin(Set<Engin> engin) {
		this.engin = engin;
	}


	@Column(name = "client", nullable = true, length = 45)
	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}
	
	@Column(name = "contactClient", nullable = true, length = 45)
	public String getContactClient() {
		return contactClient;
	}

	public void setContactClient(String contactClient) {
		this.contactClient = contactClient;
	}
	
	@Column(name = "pays", nullable = true, length = 45)
	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}
	
	@Column(name = "ville", nullable = true, length = 45)
	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	@Column(name = "budget", nullable = true, length = 45)
	public Double getBudget() {
		return budget;
	}

	public void setBudget(Double budget) {
		this.budget = budget;
	}
	
	@Column(name = "description", nullable = true, length = 255)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	  @Column(name = "date_debut", nullable = true) public Date getDateDebut() {
	  return DateDebut; }
	  
	  public void setDateDebut(Date dateDebut) { DateDebut = dateDebut; }
	  
	  @Column(name = "date_fin", nullable = true) public Date getDatefin() { return
	  Datefin; }
	  
	  public void setDatefin(Date datefin) { Datefin = datefin; }
	  
	 
	
	@Override
	public String toString() {
		return "Chantier [idChantier=" + idChantier + ", Employe=" + responsable + ", nom=" + nom + ", adresse="
				+ adresse + "]";
	}

	ChantierService service;
	public Chantier MesChantiers( Chantier chantier) {
		for(Chantier ch: service.getAllChantier() ) {
			
		}
		
		return chantier;
	}
	
	
}
