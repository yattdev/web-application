package ma.polytech.application.entity;
// Generated 15 déc. 2019 16:12:14 by Hibernate Tools 4.3.5.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * MecaReparation generated by hbm2java
 */
@Entity
@Table(name = "Meca_Reparation", catalog = "project")
public class MecaReparation implements java.io.Serializable {

	private Integer idMecaReparation;
	private Mecanicien mecanicien;
	private Reparation reparation;

	public MecaReparation() {
	}

	public MecaReparation(Reparation reparation) {
		this.reparation = reparation;
	}

	public MecaReparation(Mecanicien mecanicien, Reparation reparation) {
		this.mecanicien = mecanicien;
		this.reparation = reparation;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "idMecaReparation", unique = true, nullable = false)
	public Integer getIdMecaReparation() {
		return this.idMecaReparation;
	}

	public void setIdMecaReparation(Integer idMecaReparation) {
		this.idMecaReparation = idMecaReparation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Mecanicien")
	public Mecanicien getMecanicien() {
		return this.mecanicien;
	}

	public void setMecanicien(Mecanicien mecanicien) {
		this.mecanicien = mecanicien;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Reparation", nullable = false)
	public Reparation getReparation() {
		return this.reparation;
	}

	public void setReparation(Reparation reparation) {
		this.reparation = reparation;
	}

	@Override
	public String toString() {
		return "MecaReparation [idMecaReparation=" + idMecaReparation + ", mecanicien=" + mecanicien + ", reparation="
				+ reparation + "]";
	}

}
