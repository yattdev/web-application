package ma.polytech.application.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="Poste", catalog = "project", uniqueConstraints = @UniqueConstraint(columnNames = "nom"))
public class Poste {

	private Integer id;
	private String nom;
	private String oldNom;
	private String tag;
	private Set<Employe> employes = new HashSet<Employe>(0);

	public Poste() {
		// TODO Auto-generated constructor stub
		nom = "Non defini";
		tag = "info";
	}

	public Poste(String nom) {
		this.nom = nom;
		setTag();
	}

	public Poste(String nom, Set<Employe> employes) {
		this.nom = nom;
		this.employes = employes;
		setTag();
	}
	
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the employes
	 */
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "poste", cascade = CascadeType.ALL)
	public Set<Employe> getEmployes() {
		return employes;
	}	

	/**
	 * @param employes the employes to set
	 */
	public void setEmployes(Set<Employe> employes) {
		this.employes = employes;
	}

	/**
	 * @return the nom
	 */
	@Column(name = "nom", nullable = false)
	public String getNom() {
		return nom;
	}


	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
		setTag();
	}
	
	/**
	 * @return the oldNom
	 */
	@Column(name = "oldNom", nullable = true)
	public String getOldNom() {
		return oldNom;
	}

	/**
	 * @param oldNom the oldNom to set
	 */
	public void setOldNom(String oldNom) {
		this.oldNom = oldNom;
	}

	/**
	 * @return the tag
	 */
	@Column(name = "tag", nullable = false)
	public String getTag() {
		return tag;
	}

	/**
	 * @param tag the tag to set
	 */
	public void setTag() {
		setTag(this.nom);
	}
	
	public void setTag(String nom) {
		if(nom.contentEquals("Responsable")) {
			this.tag = "label-danger";
		}
		else if(nom.contentEquals("Conducteur")) {
			this.tag = "label-info";
		}
		else if(nom.contentEquals("Mecanicien")) {
			this.tag = "label-success";
		}
		else if(nom.contentEquals("RH")) {
			this.tag = "label-warning";
		}
		else{
			this.tag = "label-inverse";
		}
	}

	@Override
	public String toString() {
		return nom;
	}

}
