package ma.polytech.application.configuration;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String username, String password);
}

