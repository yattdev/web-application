package ma.polytech.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import ma.polytech.application.entity.Panne;
import ma.polytech.application.entity.Reparation;
import ma.polytech.application.repository.PanneRepository;
import ma.polytech.application.service.PanneService;
import ma.polytech.application.service.ReparationService;


//@EnableAutoConfiguration(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:validation");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource());
		return bean;
	}

	/*
	 * FOR CUSTOM PAGE,EW EXTEND THIS APPLICATION FOR
	 *  SpringBootServletInitializer and add this methode above
	 */ 
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(Application.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
//		Panne panne = new Panne("description panne", "panne3");
//		PanneService panneRepo = new PanneService();
//		Reparation rep = new Reparation("description reparation");
//		panne.setReparations(rep);
//		rep.setPanne(panne);
//		System.out.println(panne);
//		System.out.println(panne.getReparations());
//		panneRepo.savePanne(panne);
	}

}
