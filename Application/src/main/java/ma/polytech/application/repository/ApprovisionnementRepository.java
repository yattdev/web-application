package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Approvisionner;

public interface ApprovisionnementRepository extends JpaRepository<Approvisionner, Integer> {

}
