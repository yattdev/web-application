package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Entretien;

public interface EntretienRepository extends JpaRepository<Entretien, Integer>{

	Entretien findByTitre(String titre);

}
