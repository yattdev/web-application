package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import ma.polytech.application.entity.Chantier;

public interface ChantierRepository extends JpaRepository<Chantier, Integer>{

}
