package ma.polytech.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.Poste;

public interface EmployeRepository extends JpaRepository<Employe, Integer> {

	Employe findByUsername(String username);

	Employe findByEmail(String email);

	List<Employe> findByPoste(Poste byIdPoste);

}