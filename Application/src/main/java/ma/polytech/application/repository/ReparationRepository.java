package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Reparation;

public interface ReparationRepository extends JpaRepository<Reparation, Integer>{

}
