package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.polytech.application.entity.Panne;

@Repository
public interface PanneRepository extends JpaRepository<Panne, Integer>{


}
