package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Engin;

public interface EnginRepository extends JpaRepository<Engin, Integer>{

}
