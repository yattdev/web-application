package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Poste;

public interface PosteRepository extends JpaRepository<Poste, Integer>{

	Poste findByNom(String nom);

}
