package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Conducteur;

public interface ConducteurRepository extends JpaRepository<Conducteur, Integer>{

}
