package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Mecanicien;

public interface MecanicienRepository extends JpaRepository<Mecanicien, Integer>{

}
