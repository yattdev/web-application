package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	Role findByName(String Name);

}

