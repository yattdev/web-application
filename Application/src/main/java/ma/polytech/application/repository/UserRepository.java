package ma.polytech.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Poste;
import ma.polytech.application.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByUsername(String username);

	User findByEmail(String email);
}