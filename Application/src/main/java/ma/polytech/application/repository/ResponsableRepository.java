package ma.polytech.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.polytech.application.entity.Responsable;

public interface ResponsableRepository extends JpaRepository<Responsable, Integer>{

}
