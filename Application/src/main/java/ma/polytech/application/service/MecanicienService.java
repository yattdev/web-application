package ma.polytech.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.Mecanicien;
import ma.polytech.application.entity.Poste;
import ma.polytech.application.repository.MecanicienRepository;

@Service
public class MecanicienService {

	@Autowired
	private MecanicienRepository repository;
	
	@Autowired
	PosteService posteService;
	
	public Mecanicien save(Mecanicien meca) {
		if(posteService.findPosteByNom("Mecanicien") == null)
			posteService.savePoste(new Poste("Mecanicien"));
		meca.setPoste(posteService.findPosteByNom("Mecanicien"));
		return repository.save(meca);
	}
	
	public List<Mecanicien> save(List<Mecanicien> mecas){
		return repository.saveAll(mecas);
	}
	
	public Mecanicien getById(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Mecanicien> getAll(){
		return repository.findAll();
	}
	
	public Mecanicien update(Employe emp) {
		Mecanicien ancien = getById(emp.getId());
		ancien.setNom(emp.getNom());
		ancien.setPrenom(emp.getPrenom());
		ancien.setUsername(emp.getUsername());
		ancien.setPassword(emp.getPassword());
		ancien.setEmail(emp.getEmail());
		ancien.setPoste(emp.getPoste());
		ancien.setSalaire(emp.getSalaire());
		ancien.setDate(emp.getDate());
		ancien.setPhone(emp.getPhone());
		ancien.setAdresse(emp.getAdresse());
		
//		ancien.setReparations(emp.getReparations());
		return repository.save(ancien);
	}

	public String deleteById(int id) {
		repository.deleteById(id);
		return "Produit supprimer";
	}
	
	public String delete(Mecanicien meca) {
		return deleteById(meca.getId());
	}
	
}

