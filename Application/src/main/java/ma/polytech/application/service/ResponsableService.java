package ma.polytech.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.Poste;
import ma.polytech.application.entity.Responsable;
import ma.polytech.application.repository.ResponsableRepository;

@Service
public class ResponsableService {

	@Autowired
	private ResponsableRepository repository;
	
	@Autowired
	PosteService posteService;
	
	public Responsable save(Responsable meca) {
		if(posteService.findPosteByNom("Responsable") == null)
			posteService.savePoste(new Poste("Responsable"));
		meca.setPoste(posteService.findPosteByNom("Responsable"));
		return repository.save(meca);
	}
	
	public List<Responsable> save(List<Responsable> emps){
		return repository.saveAll(emps);
	}
	
	public Responsable getById(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Responsable> getAll(){
		return repository.findAll();
	}
	
	public Responsable update(Employe emp) {
		Responsable ancien = getById(emp.getId());
		ancien.setNom(emp.getNom());
		ancien.setPrenom(emp.getPrenom());
		ancien.setUsername(emp.getUsername());
		ancien.setPassword(emp.getPassword());
		ancien.setEmail(emp.getEmail());
		ancien.setPoste(emp.getPoste());
		ancien.setSalaire(emp.getSalaire());
		ancien.setDate(emp.getDate());
		ancien.setPhone(emp.getPhone());
		ancien.setAdresse(emp.getAdresse());
		
//		ancien.setChantier(emp.getChantier());
		
		return repository.save(ancien);
	}

	public String deleteById(int id) {
		repository.deleteById(id);
		return "Produit supprimer";
	}
	
	public String delete(Responsable emp) {
		return deleteById(emp.getId());
	}
	
}


