package ma.polytech.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.polytech.application.entity.Chantier;
import ma.polytech.application.entity.Panne;
import ma.polytech.application.repository.PanneRepository;

@Service
public class PanneService {
	
	@Autowired
	private PanneRepository repository;
	
	public Panne savePanne(Panne Panne) {
		return repository.save(Panne);
	}
	
	public List<Panne> savePannes(List<Panne> Pannes){
		return repository.saveAll(Pannes);
	}
	
	
	public Panne getByIdPanne(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Panne> getAllPanne(){
		return repository.findAll();
	}
	
	public Panne updatePanne(Panne panne) {
		Panne pan = repository.findById(panne.getIdPanne()).orElse(null);
		pan.setDescription(panne.getDescription());
		pan.setDatePanne(panne.getDatePanne());
		pan.setReparation(panne.getReparation());
		System.out.println("#######################");
		System.out.println(panne.getReparation());
		pan.setEngin(panne.getEngin());
		pan.setTitre(panne.getTitre());
		System.out.println("############ PANNE SAVED ############");
		return repository.save(pan);
	}
	
	public void update2(Panne panne) {
		System.out.println("########### DANS SAVE PANNE 2 ##########");
		System.out.println(panne.getIdPanne());
		System.out.println(repository.findById(panne.getIdPanne()));
	}
	
	
	
	public String deletePanne(int id) {
		repository.deleteById(id);
		return "Supprimer avec succes || "+id;
	}
	
}
