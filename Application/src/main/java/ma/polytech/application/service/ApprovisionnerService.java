package ma.polytech.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.polytech.application.entity.Approvisionner;
import ma.polytech.application.repository.ApprovisionnementRepository;

@Service
public class ApprovisionnerService {


	@Autowired
	private ApprovisionnementRepository repository;
	
	public Approvisionner saveApprovionnement(Approvisionner appro) {
		return repository.save(appro);
	}
	
	public List<Approvisionner> saveApprovisionnementsList(List<Approvisionner> approlist){
		return repository.saveAll(approlist);
	}
	
	public Approvisionner getByIdApprovisionner(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public Approvisionner updateApprosionner(Approvisionner approv) {
		Approvisionner appr = getByIdApprovisionner(approv.getIdApprovisionner());
		appr.setDate(approv.getDate());
		appr.setQuantite(approv.getQuantite());
		appr.setEngin(approv.getEngin());
		
		return repository.save(appr);
	}
	
	public List<Approvisionner> getAllApprovisionnement(){
		return repository.findAll();
	}
	
	public String deleteApprov(int id) {
		repository.deleteById(id);
		return "Suppression avec succes ||"+id;
	}

}
