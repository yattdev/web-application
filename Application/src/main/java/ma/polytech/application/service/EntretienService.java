package ma.polytech.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.polytech.application.entity.Entretien;
import ma.polytech.application.repository.EntretienRepository;

@Service
public class EntretienService {
	@Autowired
	private EntretienRepository repository;
	
	public Entretien saveEntretien(Entretien entretien) {
		return repository.save(entretien);
	}
	
	public List<Entretien> saveEntretiens(List<Entretien> entretiens){
		return repository.saveAll(entretiens);
	}
	
	public Entretien getById(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Entretien> getAllEntretien(){
		return repository.findAll();
	}
	
	public Entretien updateEntretien(Entretien entretien) {
		Entretien avant = getById(entretien.getIdEntretien());
		avant.setDate(entretien.getDate());
		avant.setDateProchain(entretien.getDateProchain());
		avant.setDescription(entretien.getDescription());
		//avant.setMecanicien(entretien.getMecanicien());
		avant.setEngin(entretien.getEngin());
		return repository.save(avant);
	}
	
	public String deleteEntretien(int id) {
		repository.deleteById(id);
		return "Supprimer avec succes ||"+id;
	}

	public Entretien findByTitre(String titre) {
		// TODO Auto-generated method stub
		return repository.findByTitre(titre);
	}

}
