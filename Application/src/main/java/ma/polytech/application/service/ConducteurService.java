package ma.polytech.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.polytech.application.entity.Conducteur;
import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.Poste;
import ma.polytech.application.repository.ConducteurRepository;
import ma.polytech.application.repository.EmployeRepository;

@Service
public class ConducteurService {

	@Autowired
	private ConducteurRepository repository;
	
	@Autowired
	private EmployeRepository employeRepository;
	
	@Autowired
	PosteService posteService;
	
	public Conducteur save(Conducteur meca) {
		if(posteService.findPosteByNom("Conducteur") == null)
			posteService.savePoste(new Poste("Conducteur"));
		meca.setPoste(posteService.findPosteByNom("Conducteur"));
		return repository.save(meca);
	}
	
	
	public List<Conducteur> save(List<Conducteur> emps){
		return repository.saveAll(emps);
	}
	
	public Conducteur getById(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Conducteur> getAll(){
		return repository.findAll();
	}
	
	public Conducteur update(Employe emp) {
		Conducteur ancien = getById(emp.getId());
		ancien.setNom(emp.getNom());
		ancien.setPrenom(emp.getPrenom());
		ancien.setUsername(emp.getUsername());
		ancien.setPassword(emp.getPassword());
		ancien.setEmail(emp.getEmail());
		ancien.setPoste(emp.getPoste());
		ancien.setSalaire(emp.getSalaire());
		ancien.setDate(emp.getDate());
		ancien.setPhone(emp.getPhone());
		ancien.setAdresse(emp.getAdresse());

//		ancien.setEngin(emp.getEngin());
		
		return repository.save(ancien);
	}

	public String deleteById(int id) {
		repository.deleteById(id);
		return "Produit supprimer";
	}
	
	public String delete(Conducteur emp) {
		return deleteById(emp.getId());
	}
	
}

