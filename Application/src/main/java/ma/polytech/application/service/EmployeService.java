package ma.polytech.application.service;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import javax.mail.SendFailedException;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import ma.polytech.application.entity.Conducteur;
import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.Mecanicien;
import ma.polytech.application.entity.Poste;
import ma.polytech.application.entity.Responsable;
import ma.polytech.application.entity.User;
import ma.polytech.application.mailSender.SendEmailHTML;
import ma.polytech.application.repository.ConducteurRepository;
import ma.polytech.application.repository.EmployeRepository;
import ma.polytech.application.repository.MecanicienRepository;
import ma.polytech.application.repository.PosteRepository;
import ma.polytech.application.repository.ResponsableRepository;
import ma.polytech.application.repository.RoleRepository;
import ma.polytech.application.repository.UserRepository;
import ma.polytech.application.validator.UserValidator;

@Service
public class EmployeService {

	@Autowired
	private EmployeRepository repository;
	
	@Autowired
	private MecanicienService mecanicienService;
	
	@Autowired
	private ConducteurService conducteurService;
	
	@Autowired
	private ResponsableService responsableService;
	
	@Autowired
	private PosteRepository posteRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserServiceImp userService;
	
	@Autowired
	private RoleRepository roleRepository;
	
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;	
	
    @Autowired
    private UserValidator userValidator;
    
	public Employe findByUsername(String username) {
		return repository.findByUsername(username);
	}

	@Async
	public Employe save(Employe emp, BindingResult bindingResult){
		// Create Login User
		User user = new User();
		
		user.setUsername(emp.getUsername());
		user.setEmail(emp.getEmail());
		user.setPassword(emp.getPassword());
		user.setAgreeTerms(true);
		if(emp.getPoste() == null)
			if(posteRepository.findByNom("Non defini") != null)
				emp.setPoste(posteRepository.findByNom("Non defini"));
			else {
				Poste p = new Poste();
				posteRepository.save(p);
				emp.setPoste(p);
			}

		if(emp.getPoste().getNom().contentEquals("Responsable"))
			user.setRoles(Arrays.asList(roleRepository.findByName("ROLE_ADMIN")));
		else
			user.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));
//		user = userService.save(user, bindingResult);

		/* SEND MAIL TO USER CREATED */
		sendMailEmploye(emp);
		
//		if(passwordEncoder.upgradeEncoding(emp.getPassword()))
		emp.setPassword(passwordEncoder.encode(emp.getPassword()));

		if(emp.postNom().contentEquals("Responsable")) {
			Responsable newResponsable = new Responsable(emp);
			return (responsableService.save(newResponsable));
		}
		else if(emp.postNom().contentEquals("Mecanicien")) {
			Mecanicien newMecanicien = new Mecanicien(emp);
			return (mecanicienService.save(newMecanicien));
		}
		else if(emp.postNom().contentEquals("Conducteur")) {
			Conducteur newConducteur = new Conducteur(emp);
			return (conducteurService.save(newConducteur));
		}
		
		return (repository.save(emp));
	}
	
    @Async	
    public CompletableFuture<Employe> sendMailEmploye(Employe emp) {
        String emailText = "<h1>Hello " + emp.getUsername() + "</h1>\n\n"
        		+ "<h3>Your are Sign up like Employe on YATTCORP successfully....</h3>\nYour login identifiant:\n"
        		+ "<h4>Username: "+emp.getUsername()+"</h4>\n"
        		+ "<h4>Password: "+emp.getPassword()+"</h4>";
        SendEmailHTML sendEmailHtml = new SendEmailHTML("yattdeveloper@gmail.com", "Mon+@ge=22_00", emp.getEmail(), emailText);
        sendEmailHtml.sendMail();
        return CompletableFuture.completedFuture(emp);
    }
	
    @Async
	public Employe save(Employe emp){
		// ENCODE PASSWORD
//		if(passwordEncoder.upgradeEncoding(emp.getPassword()))
		emp.setPassword(passwordEncoder.encode(emp.getPassword()));
		
		if(emp.getPoste() == null) {
			if(posteRepository.findByNom("Non defini") != null)
				emp.setPoste(posteRepository.findByNom("Non defini"));
			else {
				Poste p = new Poste();
				posteRepository.save(p);
				emp.setPoste(p);
			}
		}
		else if(emp.postNom().contentEquals("Responsable")) {
			Responsable newResponsable = new Responsable(emp);
			return responsableService.save(newResponsable);
		}
		else if(emp.postNom().contentEquals("Mecanicien")) {
			Mecanicien newMecanicien = new Mecanicien(emp);
			return mecanicienService.save(newMecanicien);
		}
		else if(emp.postNom().contentEquals("Conducteur")) {
			Conducteur newConducteur = new Conducteur(emp);
			return conducteurService.save(newConducteur);
		}

		// Save empploye created
        Employe empSaved = repository.save(emp);
        return empSaved;
	}
	
	public List<Employe> saves(List<Employe> emps){
		return repository.saveAll(emps);
	}
	
	public Employe getById(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Employe> getAll(){
		return repository.findAll();
	}
	
	@Async
	public Employe update(Employe emp, BindingResult bindingResult) {
		User user = userRepository.findByEmail(emp.getEmail());
		if(user != null) {
			user.setUsername(emp.getUsername());
			user.setEmail(emp.getEmail());
			user.setPassword(emp.getPassword());
			if(emp.getPoste().getNom().contentEquals("Responsable")) {
				user.setRoles(roleRepository.findAll());
			}
			user = userService.update(user, bindingResult);
		}

		
		// Update Employe
		Employe ancien = repository.findById(emp.getId()).orElse(null);
		Poste post = posteRepository.findById(ancien.getPoste().getId()).orElse(null);
		String postNom = post.getNom();
		if(passwordEncoder.matches(emp.getPassword(), ancien.getPassword()) == false)
			emp.setPassword(passwordEncoder.encode(emp.getPassword()));

		ancien.setNom(emp.getNom());
		ancien.setPrenom(emp.getPrenom());
		ancien.setUsername(emp.getUsername());
		ancien.setEmail(emp.getEmail());
		ancien.setPassword(emp.getPassword());
		ancien.setPoste(emp.getPoste());
		ancien.setSalaire(emp.getSalaire());
		ancien.setDate(emp.getDate());
		ancien.setPhone(emp.getPhone());
		ancien.setAdresse(emp.getAdresse());
		
		/* SEND MAIL TO USER CREATED */
		sendMailEmploye(emp);
		
		if(emp.postNom().contentEquals("Responsable")) {
			if(postNom.contentEquals("Responsable")) {
//				Responsable newResponsable = responsableService.getById(emp.getId());
				return (responsableService.update(emp));
			}
			deleteById(emp.getId());
			Responsable newResponsable = new Responsable(emp);
			System.out.println(newResponsable);
			return (responsableService.save(newResponsable));
		}
		else if(emp.postNom().contentEquals("Mecanicien")) {
			if(postNom.contentEquals("Mecanicien")) {
				return (mecanicienService.update(emp));
			}
			deleteById(emp.getId());
			Mecanicien newMecanicien = new Mecanicien(emp);
			System.out.println(newMecanicien);
			return (mecanicienService.save(newMecanicien));
		}
		else if(emp.postNom().contentEquals("Conducteur")) {
			if(postNom.contentEquals("Conducteur")) {
				return (conducteurService.update(emp));
			}
			deleteById(emp.getId());
			Conducteur newConducteur = new Conducteur(emp);
			System.out.println(newConducteur);
			return (conducteurService.save(newConducteur));
		}
		
		return (repository.save(ancien));
	}
	
	public String deleteById(int id) {
		repository.deleteById(id);
		if(userService.getById(id) != null)
			userService.deleteById(id);
		return "Employe supprimer";
	}
	
	public String delete(Employe emp) {
		if(userService.getById(emp.getId()) != null)
			userService.deleteById(emp.getId());
		return deleteById(emp.getId());
	}
	/*
	 * @PreRemove public String deleteUuser(int id) { return
	 * userService.deleteById(id); }
	 */

	public Employe findByEmail(String email) {
		// TODO Auto-generated method stub
		return repository.findByEmail(email);
	}


	public List<Employe> findByPoste(Poste byIdPoste) {
		// TODO Auto-generated method stub
		return repository.findByPoste(byIdPoste);
	}

}
