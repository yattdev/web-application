package ma.polytech.application.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ma.polytech.application.entity.Employe;

public interface EmployeDetailsService extends UserDetailsService{
	
    Employe findByEmail(String email);

    Employe save(Employe registration);

}
