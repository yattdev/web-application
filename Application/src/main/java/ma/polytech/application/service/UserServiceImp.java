package ma.polytech.application.service;

import java.util.ArrayList;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.mail.SendFailedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import ma.polytech.application.entity.Role;
import ma.polytech.application.entity.User;
import ma.polytech.application.exception.EmailExistsException;
import ma.polytech.application.exception.UsernameExistsException;
import ma.polytech.application.mailSender.SendEmailHTML;
import ma.polytech.application.repository.RoleRepository;
import ma.polytech.application.repository.UserRepository;
import ma.polytech.application.validator.UserValidator;


@Service
public class UserServiceImp implements UserService{

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private UserValidator userValidator;
	
	public UserServiceImp() {
		// TODO Auto-generated constructor stub
	}

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
            user.getPassword(), mapRolesToAuthorities(user.getRoles()));
    }
//
//    private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {
//		return getGrantedAuthorities(getPrivileges(roles));
//	}
    
    private Collection < ? extends GrantedAuthority > mapRolesToAuthorities(Collection < Role > roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName()))
            .collect(Collectors.toList());
    }
	
	public User save(User emp, BindingResult bindingResult) {
		/*VALIDATE USER BEFORE SAVE IT*/
		userValidator.validate(emp, bindingResult);
		if(bindingResult.hasFieldErrors("email") == false){
			if(bindingResult.hasFieldErrors("username") == true) {
				emp.setUsername(emp.getUsername() + "-Employe");
			}
			return save(emp);
		}
		// UPDATE EMPLOYE IF EXISTE
		
		return update(findByEmail(emp.getEmail()), bindingResult);
	}
	
	@Override
	public User save(User emp) {
		if(emp.getRoles() == null) {
			emp.setRoles(roleRepository.findAll());
		}
		String pass = emp.getPassword();
		emp.setPassword(passwordEncoder.encode(emp.getPassword()));
		User empSaved = userRepository.save(emp);
		sendMailUser(empSaved, pass);
        return empSaved;
	}
	
	public void sendMailUser(User empSaved, String pass) {
        String emailText = "<h1>Hello " + empSaved.getUsername() + "</h1>\n\n"
        		+ "Your are Sign up like USER on <h3>YATTCORP,</h3> successfully....\nYour login identifiant:\n"
        		+ "<h5>Username: </h5>"+empSaved.getUsername()+"\n"
        		+ "<h5>Username: </h5>"+pass;
        SendEmailHTML sendEmailHtml = new SendEmailHTML("yattdeveloper@gmail.com", "Mon+@ge=22_00", empSaved.getEmail(), emailText);
        sendEmailHtml.sendMail();
	}
	
	public List<User> saves(List<User> emps){
		return userRepository.saveAll(emps);
	}
	
	public User getById(int id) {
		return userRepository.findById(id).orElse(null);
	}
	
	public List<User> getAll(){
		return userRepository.findAll();
	}
	
	public User update(User emp, BindingResult bindingResult) {
		/*VALIDATE USER BEFORE SAVE IT*/
		userValidator.validateUpdate(emp, bindingResult);
		if(bindingResult.hasFieldErrors("username") == true)
			emp.setUsername(emp.getUsername() + "-Employe");
		
		User ancien = getById(emp.getId());
		if(passwordEncoder.matches(emp.getPassword(), ancien.getPassword()) == false)
			emp.setPassword(passwordEncoder.encode(emp.getPassword()));
		ancien.setUsername(emp.getUsername());
		ancien.setPassword(emp.getPassword());
		ancien.setEmail(emp.getEmail());
		ancien.setRoles(emp.getRoles());
		
		return userRepository.save(ancien);
	}

	public String deleteById(int id) {
		userRepository.deleteById(id);
		return "Produit supprimer";
	}
	
	public String delete(User emp) {
		return deleteById(emp.getId());
	}

	public User findByEmail(String email) {
		// TODO Auto-generated method stub
		return userRepository.findByEmail(email);
	}

	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return userRepository.findByUsername(username);
	}
	
	/* ##################### NOT UTILE POUR LE MOMENT #################################### */
	//	@Override
//	public User registerNewUserAccount(User account) throws UsernameExistsException, EmailExistsException{
//		
//	    if (usernameExist(account.getEmail())) {
//	        throw new UsernameExistsException
//	          ("There is an account with that username: " + account.getUsername());
//	    }
//	  
//	    if (emailExist(account.getEmail())) {
//	        throw new EmailExistsException
//	          ("There is an account with that email adress: " + account.getEmail());
//	    }
//	    User user = new User();
//	 
//	    user.setUsername(account.getUsername());
//	    user.setPassword(passwordEncoder.encode(account.getPassword()));
//	    user.setEmail(account.getEmail());
//	    System.out.println(account.getPassword());
//	 
//	    user.setRoles(Arrays.asList(roleRepository.findByName("ROLE_USER")));
//	    return userRepository.save(user);
//	}
//
//	
//	private boolean usernameExist(String username) {
//		// TODO Auto-generated method stub
//		for (User user : userRepository.findAll()) {
//			if(user.getUsername().contentEquals(username))
//				return true;
//		}
//		return false;
//	}
//
//	private boolean emailExist(String email) {
//		// TODO Auto-generated method stub
//		for (User user : userRepository.findAll()) {
//			if(user.getEmail().contentEquals(email))
//				return true;
//		}
//		return false;
//	}
//
//	public User findByUsername(String username) {
//		return userRepository.findByUsername(username);
//	}

	
}