package ma.polytech.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.polytech.application.entity.Poste;
import ma.polytech.application.repository.PosteRepository;

@Service
public class PosteService {
	
	@Autowired
	private PosteRepository repository;
	
	public Poste savePoste(Poste poste) {
		poste.setTag();
		return repository.save(poste);
	}
	
	public List<Poste> savePostes(List<Poste> Postes){
		return repository.saveAll(Postes);
	}
	
	
	public Poste getByIdPoste(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Poste> getAllPoste(){
		return repository.findAll();
	}
	
	public Poste updatePoste(Poste poste) {
		Poste p = getByIdPoste(poste.getId());
		p.setNom(poste.getNom());
		
		return repository.save(p);
	}
	
	public String deletePoste(Poste poste) {
		repository.delete(poste);
		return "Supprimer avec succes !";
	}
	
	public String deleteByid(int id) {
		repository.deleteById(id);
		return "Supprimer avec succes || "+id;
	}

	public Poste findPosteByNom(String nom) {
		// TODO Auto-generated method stub
		return repository.findByNom(nom);
	}
	
}

