package ma.polytech.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.polytech.application.entity.Reparation;
import ma.polytech.application.repository.ReparationRepository;

@Service
public class ReparationService {

	@Autowired
	private ReparationRepository repository;
	
	@Autowired
	private PanneService pservice;
	
	public Reparation save(Reparation rep) {
		return repository.save(rep);
	}
	
	public List<Reparation> save(List<Reparation> reps){
		return repository.saveAll(reps);
	}
	
	public Reparation getById(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Reparation> getAll(){
		return repository.findAll();
	}
	
	public Reparation update(Reparation rep) {
		
		pservice.getAllPanne();
		getAll();
		Reparation ancien = getById(rep.getIdReparation());
		
		//Reparation ancien = repository.findById(rep.getIdReparation()).orElse(null);
		ancien.setPanne(rep.getPanne());
		ancien.setCoutDeReparation(rep.getCoutDeReparation());
		ancien.setDateReparation(rep.getDateReparation());
		ancien.setDescription(rep.getDescription());
		ancien.setMecanicien(rep.getMecanicien());
		
		return repository.save(ancien);
	}

	public String deleteById(int id) {
		repository.deleteById(id);
		return "Produit supprimer";
	}
	
	public String delete(Reparation rep) {
		return deleteById(rep.getIdReparation());
	}
	
}

