package ma.polytech.application.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.polytech.application.entity.Chantier;
import ma.polytech.application.repository.ChantierRepository;

@Service
public class ChantierService {
	@Autowired
	private ChantierRepository repository;
	
	public Chantier saveChantier(Chantier chantier) {
		return repository.save(chantier);
	}
	
	public List<Chantier> savechantiers(List<Chantier> chantiers){
		return repository.saveAll(chantiers);
	}
	
	
	public Chantier getByIdChantier(int id) {
		return repository.findById(id).orElse(null);
	}
	
	/*
	 * public Chantier getByNameChantier(Chantier name) { return
	 * repository.findByName(name); }
	 */
	
	public List<Chantier> getAllChantier(){
		return repository.findAll();
	}
	
	public Chantier updateChantier(Chantier chantier) {
		Chantier chant = repository.findById(chantier.getIdChantier()).orElse(null);
		chant.setAdresse(chantier.getAdresse());
		chant.setNom(chantier.getNom());
		chant.setResponsable(chantier.getResponsable());
		chant.setEngin(chantier.getEngin());
		chant.setAdresse(chantier.getAdresse());
		chant.setBudget(chantier.getBudget());
		chant.setContactClient(chantier.getContactClient());
		chant.setClient(chantier.getClient());
		chant.setDescription(chantier.getDescription());
		chant.setPays(chantier.getPays());
		chant.setVille(chantier.getVille());
		chant.setDateDebut(chantier.getDateDebut());
		chant.setDatefin(chantier.getDatefin());
		return repository.save(chant);
	}
	
	
	public String deleteChant(int id) {
		repository.deleteById(id);
		return "Supprimer avec succes || "+id;
	}

	public Object findByName(String nom) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*
	 * @Override
	 * 
	 * @Cacheable("fetchChantier") public List<Chantier> fetchChantier(String
	 * searchTerm) throws Exception { // TODO Auto-generated method stub return
	 * repository.fetch(searchTerm);
	 * 
	 * }
	 */

}
