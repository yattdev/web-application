package ma.polytech.application.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.polytech.application.entity.Chantier;
import ma.polytech.application.entity.Engin;
import ma.polytech.application.repository.EnginRepository;

@Service
public class EnginService {
	@Autowired
	private EnginRepository  repository;
	
	public Engin saveEngin(Engin engin) {
		return repository.save(engin);
	}
	
	public List<Engin> saveEngins(List<Engin> engins){
		return repository.saveAll(engins);
	}
	
	public Engin getById(int id) {
		return repository.findById(id).orElse(null);
	}
	
	public List<Engin> getAll(){
		return repository.findAll();
	}
	
	public Engin updateEngin(Engin engin) {
		Engin eg = repository.findById(engin.getIdEngin()).orElse(null);
		eg.setRef(engin.getRef());
		eg.setDateCirculation(engin.getDateCirculation());
		eg.setTypeEngin(engin.getTypeEngin());
		eg.setChantier(engin.getChantier());
		eg.setEntretien(engin.getEntretien());
		eg.setPanne(engin.getPanne());
		eg.setConducteur(engin.getConducteur());
		return repository.save(eg);
	}
	
	
	public String deleteEngin(int id) {
		repository.deleteById(id);
		return "Engin supprimer avec succes ||" +id;
	}

}
