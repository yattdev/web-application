package ma.polytech.application.service;


import org.springframework.security.core.userdetails.UserDetailsService;

import ma.polytech.application.entity.User;

public interface UserService extends UserDetailsService{
	
    User findByEmail(String email);

    User save(User registration);
    
//	public User registerNewUserAccount(User account);
}
