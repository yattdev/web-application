package ma.polytech.application.mailSender;

import javax.activation.DataHandler;

import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.scheduling.annotation.Async;

import com.sun.mail.smtp.SMTPAddressFailedException;
import com.sun.mail.smtp.SMTPTransport;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

public class SendEmailHTML {

    // for example, smtp.mailgun.org
    private String SMTP_SERVER = "smtp.gmail.com";
    private String USERNAME = "yattdeveloper";
    private String PASSWORD = "";

    private String EMAIL_FROM = "yattdeveloper@gmail.com";
    private String EMAIL_TO = "email_1@yahoo.com, email_2@gmail.com";
    private String EMAIL_TO_CC = "";

    private String EMAIL_SUBJECT = "Test Send Email via SMTP (HTML)";
    private String EMAIL_TEXT = "<h1>Hello Java Mail \n ABC123</h1>";

    public SendEmailHTML(String username, String password, String emailTo, String emailText) {
    	this.USERNAME = username;
    	this.PASSWORD = password;
    	this.EMAIL_TO = emailTo;
    	this.EMAIL_TEXT = emailText;
    }
    
    @Async
    public void sendMail(){

        //Get properties object    
        Properties prop = new Properties();    
        prop.put("mail.smtp.host", "smtp.gmail.com");    
        prop.put("mail.smtp.socketFactory.port", "465");    
        prop.put("mail.smtp.socketFactory.class",    
                  "javax.net.ssl.SSLSocketFactory");    
        prop.put("mail.smtp.auth", "true");    
        prop.put("mail.smtp.port", "465");

        //get Session   
        Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
         protected PasswordAuthentication getPasswordAuthentication() {    
         return new PasswordAuthentication(EMAIL_FROM, PASSWORD);  
         }    
        });   
        
        Message msg = new MimeMessage(session);

        try {

            msg.setFrom(new InternetAddress(EMAIL_FROM));

            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_TO, false));

            msg.setSubject(EMAIL_SUBJECT);

			// TEXT email
            //msg.setText(EMAIL_TEXT);

			// HTML email
            msg.setDataHandler(new DataHandler(new HTMLDataSource(EMAIL_TEXT)));

            Transport.send(msg);  
            System.out.println("message sent successfully");    

//			SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
//
//			// connect
//            t.connect(SMTP_SERVER, USERNAME, PASSWORD);
//
//			// send
//            t.sendMessage(msg, msg.getAllRecipients());

//            System.out.println("Response: " + t.getLastServerResponse());

//            t.close();


        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }

    static class HTMLDataSource implements DataSource {

        private String html;

        public HTMLDataSource(String htmlString) {
            html = htmlString;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            if (html == null) throw new IOException("html message is null!");
            return new ByteArrayInputStream(html.getBytes());
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            throw new IOException("This DataHandler cannot write HTML");
        }

        @Override
        public String getContentType() {
            return "text/html";
        }

        @Override
        public String getName() {
            return "HTMLDataSource";
        }
    }
}