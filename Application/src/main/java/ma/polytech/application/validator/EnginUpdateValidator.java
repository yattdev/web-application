package ma.polytech.application.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ma.polytech.application.entity.Entretien;
import ma.polytech.application.service.EntretienService;

@Component
public class EnginUpdateValidator implements Validator {
	@Autowired
	private EntretienService service;
	
	
    @Override
    public boolean supports(Class<?> aClass) {
        return Entretien.class.equals(aClass);
    }	

    @Override
    public void validate(Object o, Errors errors) {
        Entretien entretien = (Entretien) o;

        System.out.println("############# MODIFIER CHANTIER ############################");
        if (entretien.getTitre().length() < 6) {
            errors.rejectValue("titre", "Titre.entretienForm.titre");
			System.out.println("####################### ERROR TITRE TOO SHOR ################");
        }

        if (entretien.getDate().compareTo(entretien.getDateProchain())>0) {
			System.out.println("####################### ERROR IN DATE FIELD ################");
            errors.rejectValue("date", "DateError.entretienForm.date");
            errors.rejectValue("dateProchain", "DateError.entretienForm.dateProchain");
        }
    }
    
}

