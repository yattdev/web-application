package ma.polytech.application.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ma.polytech.application.entity.Employe;
import ma.polytech.application.entity.User;
import ma.polytech.application.service.EmployeService;
import ma.polytech.application.service.UserServiceImp;

@Component
public class EmployeValidator implements Validator {
	@Autowired
	private EmployeService employeService;
	
    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }	

    @Override
    public void validate(Object o, Errors errors) {
        Employe user = (Employe) o;

        System.out.println("############# DANS INSCRIPTION ############################");
        if (user.getUsername().length() < 4 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        }
        
        if (employeService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        if (user.getPassword().length() < 6 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        }

        if (user.getEmail().length() < 8 || !user.getEmail().contains("@")) {
            errors.rejectValue("email", "Invalide.userForm.email");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        }

        if (employeService.findByEmail(user.getEmail()) != null) {
            errors.rejectValue("email", "Duplicate.userForm.email");
        }
        
        if(user.getDate() == null) {
            errors.rejectValue("date", "Invalide.userForm.date");
        }
        
        if(user.getPhone() == null) {
            errors.rejectValue("phone", "Invalide.userForm.phone");
        }
    }
    
  
    public void validateUpdate(Object o, Errors errors) {
        Employe user = (Employe) o;

        System.out.println("############# DANS VALIDATION ############################");
        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        }
        
        if (employeService.findByUsername(user.getUsername()) != null && 
        		employeService.findByUsername(user.getUsername()).getId() != user.getId()) {
			System.out.println("############# USERNAME ############################");
			System.out.println(user.getId());
			System.out.println(employeService.findByUsername(user.getUsername()).getId());
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        if (user.getPassword().length() < 6) {
            errors.rejectValue("password", "Size.userForm.password");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        }

        if (user.getEmail().length() < 8 || !user.getEmail().contains("@")) {
            errors.rejectValue("email", "Invalide.userForm.email");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        }

        if (employeService.findByEmail(user.getEmail()) != null && 
        		employeService.findByEmail(user.getEmail()).getId() != user.getId()) {
            errors.rejectValue("email", "Duplicate.userForm.email");
        }
        
        if(user.getDate() == null) {
            errors.rejectValue("date", "Invalide.userForm.date");
        }
        
        if(user.getPhone() == null) {
            errors.rejectValue("phone", "Invalide.userForm.phone");
        }
    }
    
}

