package ma.polytech.application.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ma.polytech.application.entity.Chantier;
import ma.polytech.application.service.ChantierService;

@Component
public class ChantierValidate implements Validator {
	@Autowired
	private ChantierService service;
	
	
    @Override
    public boolean supports(Class<?> aClass) {
        return Chantier.class.equals(aClass);
    }	

    @Override
    public void validate(Object o, Errors errors) {
        Chantier chantier = (Chantier) o;

        System.out.println("############# MODIFIER CHANTIER ############################");
        if (chantier.getNom().length() < 6 || chantier.getNom().length() > 20) {
            errors.rejectValue("nom", "Size.chantier.nom");
        }
        
        if (service.findByName(chantier.getNom()) != null) {
            errors.rejectValue("nom", "Exist.chantier.nom");
        }

        if (chantier.getAdresse().length() < 6 || chantier.getAdresse().length() > 32) {
            errors.rejectValue("adresse", "Size.chantierForm.password");
        }
       
		if(chantier.getDateDebut().compareTo(chantier.getDatefin())>0) {
            errors.rejectValue("DateDebut", "Error.chantier.DateDebut");
		}
    }
    
    
}

