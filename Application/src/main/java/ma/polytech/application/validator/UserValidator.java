package ma.polytech.application.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import ma.polytech.application.entity.User;
import ma.polytech.application.service.UserServiceImp;

@Component
public class UserValidator implements Validator {
	@Autowired
	private UserServiceImp employeService;
	
    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }	

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;

        System.out.println("############# DANS INSCRIPTION ############################");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username");
        }
        
        if (employeService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        if (user.getEmail().length() < 8 || !user.getEmail().contains("@")) {
            errors.rejectValue("email", "Invalide.userForm.email");
        }
        
        if (employeService.findByEmail(user.getEmail()) != null) {
            errors.rejectValue("email", "Duplicate.userForm.email");
        }
        
		/* AGREE ALL TERMS VALIDATION */
        if(user.getAgreeTerms() == false) {
            errors.rejectValue("agreeTerms", "TermsNotAgree.userForm.agreeTerms");
        }
        
    }
    
    public void validateUpdate(Object o, Errors errors) {
        User user = (User) o;

        System.out.println("############# DANS INSCRIPTION ############################");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
        if (user.getUsername().length() < 6 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username");
        }
        
        if (employeService.findByUsername(user.getUsername()) != null &&
        		employeService.findByUsername(user.getUsername()).getId() != user.getId()) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (user.getPassword().length() < 8 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty");
        if (user.getEmail().length() < 8 || !user.getEmail().contains("@")) {
            errors.rejectValue("email", "Invalide.userForm.email");
        }

        if (employeService.findByEmail(user.getEmail()) != null &&
        		employeService.findByEmail(user.getEmail()).getId() != user.getId()) {
            errors.rejectValue("email", "Duplicate.userForm.email");
        }
    }
    
}
